'''
SQL Alchemy: - este un ORM - Object Relational Mapping - o reprezentare a obiectelor in Python sub o alta forma (cel mai popular);
             - nu foloseste sintaxa de SQL ci doar de Python;
             - clasa reprezinta un tabel;
             - o instanta reprezinta un rand;
             - atributele instantelor reprezinta coloanele.

SQL Alchemy sessionmaker() reprezinta comunicarea si manioularea cu baze de date, reprezentata de un cursor IN PyMySql.

pip install sqlalchemy - instaleaza pachetul
pip install pymysql

Tabelele sunt reprezentate de clase, clasele pentru a putea fi luate in considerare trebuie sa mosteneasca declarative_base()
'''
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from models import Base, Student

CONNECTION_STRING = 'mysql+pymysql://{user}:{password}@{host}/{db}'
eng = create_engine(CONNECTION_STRING.format(user='root', password='Blackmetal15$', host='localhost', db='students_db'))
# create_engine realizeaza o conexiune catre baza de date (students_db).

#creaza tabele in baza de date:
Base.metadata.create_all(eng)

Session = sessionmaker(bind=eng)   # ne da accesul la sesiune
s = Session()

# s.add_all(
#     [
#         Student(first_name='Adrian', last_name='Benea'),
#         Student(first_name='Antonia', last_name='Tudor'),
#         Student(first_name='Horia', last_name='Brenciu')
#     ]
# )
# s.commit()    # commit() se foloseste la fiecare modificare de db (insert/update/delete)

# SELECT * FROM students - pt MySQL
all_students = s.query(Student).all()    # pt SQLAlchemy
for student in all_students:
    print(f'Full name: {student.first_name} {student.last_name}')

# SELECT count(*) from students
count_students=s.query(Student).count()
print(f'Total: {count_students}')

# SELECT first_name, last_name FROM students
all_students = s.query(Student.first_name, Student.last_name).all()
print(all_students)
# SELECT * FROM students WHERE id >= 1 AND first_name LIKE 'Iu%';
results = s.query(Student).filter(Student.student_id >= 1, Student.first_name.like('Ad%')).all()
for result in results:
    print(result)

