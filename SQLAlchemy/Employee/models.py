from sqlalchemy import Column, Integer, String, Date, ForeignKey, BigInteger, Boolean
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class Departament(Base):
    __tablename__ = "departamente"
    id = Column(Integer, primary_key=True, autoincrement=True)
    nume = Column(String(255), nullable=False)

    def __str__(self):
        return f'Departament {self.nume}'


class Angajat(Base):
    __tablename__ = "angajati"
    id = Column(Integer, primary_key=True, autoincrement=True)
    cnp = Column(BigInteger, nullable=False)
    nume = Column(String(30), nullable=False)
    prenume = Column(String(30), nullable=False)
    data_angajarii = Column(Date, nullable=False)      # se poate folosi si DateTime
    departament = Column(Integer, ForeignKey(Departament.id), nullable=False)
    salariu = Column(BigInteger, nullable=False)
    manager = Column(Boolean)

    def __str__(self):
        return f'Angajatul: {self.nume} {self.prenume}'


class Bonus(Base):
    __tablename__ = 'bonusuri'

    id = Column(Integer, primary_key=True, autoincrement=True)
    data_acordarii = Column(Date)
    beneficiar = Column(Integer, ForeignKey(Angajat.id), nullable=False)
    valoare = Column(Integer)

    def __str__(self):
        return f'Bonusul in valoare de {self.valoare} acordat angajatului {self.beneficiar}'


