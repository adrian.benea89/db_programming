import datetime

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models import Base, Departament, Angajat, Bonus

CONNECTION_STRING = 'mysql+pymysql://{user}:{password}@{host}/{db}'
eng = create_engine(CONNECTION_STRING.format(user='root', password='Blackmetal15$', host='localhost', db='employees_db'))

Base.metadata.create_all(eng)

Session = sessionmaker(bind=eng)
s = Session()

# s.add_all([
#     Departament(nume="IT"),
#     Departament(nume='Contabilitate'),
#     Departament(nume='HR'),
#     Departament(nume="Marketing")
# ]
# )
# s.commit()
# s.add_all([Angajat(cnp=1234567897841, nume="Benea", prenume="Adrian",data_angajarii=datetime.date(2020,4,15),departament=1, salariu=5000, manager=True),
#            Angajat(cnp=5597894591136, nume = "Popescu", prenume="Andi", data_angajarii=datetime.date(2019,6,22), departament=2, salariu=3500, manager=True),
#            Angajat(cnp=55887966, nume = "Tudor", prenume="Gheorghe", data_angajarii=datetime.date(2018,7,7), departament= 3, salariu=6000, manager=False)])
#
# s.commit()
#
# s.add_all([Bonus(data_acordarii=datetime.date(2020, 6, 15), beneficiar=1, valoare=500),
#         Bonus(data_acordarii=datetime.date(2019, 8, 22), beneficiar=2, valoare=1000),
#         Bonus(data_acordarii=datetime.date(2018, 9, 7), beneficiar=2, valoare=2000)
# ])
#
# s.commit()

salariu_maxim = s.query(Angajat).filter(Angajat.salariu).order_by(Angajat.salariu.desc()).first()   # in loc de first se poate scrie limit(1)
print(salariu_maxim)




def adauga_angajat():
    cnp_input = int(input('Introduceti CNP: '))
    nume_input = input('Introduceti nume: ')
    prenume_input = input('Introduceti prenume: ')
    data_angajarii_input = datetime.datetime.now().date()
    departament_input = input('Introduceti departamentul: ')
    salariu_input = int(input("Introduceti salariul: "))
    manager_input = bool(input("Manager? 0/1: "))
    s.add(
        Angajat(cnp=cnp_input, nume=nume_input, prenume=prenume_input, data_angajarii=data_angajarii_input, departament=departament_input, salariu=salariu_input, manager=manager_input)
    )
    s.commit()

#adauga_angajat()

def update_angajat(id_angajat, id_dep, is_manager):
    angajat_curent = s.query(Angajat).filter(Angajat.id==id_angajat).first()
    angajat_curent.departament = id_dep
    angajat_curent.manager = is_manager
    s.commit()

update_angajat(5, 3, 0)

