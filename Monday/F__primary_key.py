import mysql.connector as mysql

db = mysql.connect(host='localhost', user='root', password='Blackmetal15$', database='DBPython')
cursor = db.cursor()
cursor.execute('DROP TABLE users')  # stergem tabela 'users' din baza de date (DBPython)

cursor.execute('CREATE TABLE users (ID INTEGER(11) AUTO_INCREMENT NOT NULL PRIMARY KEY, name VARCHAR(255), user_name VARCHAR (255))')

cursor.execute('DESC users')
print(cursor.fetchall())

cursor.execute('ALTER TABLE users DROP ID')  # dropping the id column
cursor.execute('DESC users')
print(cursor.fetchall())

# adding id column to the users table
# 'FIRST' keyword in the statement will add the column at the begin of the table
cursor.execute('ALTER TABLE users ADD COLUMN id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST')
cursor.execute('DESC users')
print(cursor.fetchall())

