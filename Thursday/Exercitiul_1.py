import mysql.connector as mysql

'''-Create a todo_app database

Create a db_tasks datatable
Create a tasks table with the following schema
○ id int not null auto_increment
○ task text not null
○ done boolean
○ primary key - id

 In a loop:
○ ask user what to do using input()
○ show task list
○ mark task as done
○ add new task
○ exit application

Implement functions which perform the above actions using the database as task storage

For show tasks:
○ print all open tasks and their ids in order of ids

For mark as done
○ ask user which id to mark as done
○ update the done field in the table for given id

For add new task
○ ask for task name/description
○ insert a new record to the tasks db'''



db = mysql.connect(host='localhost', user='root', password='Blackmetal15$', database='db_tasks')


def create_db():
    with db.cursor() as c:
        c.execute('CREATE DATABASE IF NOT EXISTS db_tasks')
        c.close()


def create_table():
    with db.cursor() as c:
        c.execute('CREATE TABLE IF NOT EXISTS db_tasks.tasks ('
                  'id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,'
                  'task TEXT NOT NULL,'
                  'done BOOLEAN DEFAULT 0)')
        c.close()


def show_task_list():
    with db.cursor() as c:
        c.execute('SELECT * FROM tasks')
        results = c.fetchall()
        for result in results:
            print(result)
        print('-' * 20)


def add_task():
    add_task = input('Introdu taskul: ')
    with db.cursor() as c:
        c.execute('''INSERT INTO tasks (task) VALUES (%s)''', (add_task,))
        print('Task adaugat!')


def mark_as_done():
    task_id = input('Introdu id-ul taskului: ')
    with db.cursor() as c:
        c.execute('UPDATE tasks SET done=1 WHERE id=%s AND done=0', (task_id,))
        db.commit()


def menu():
    print('1. Add task.')
    print('2. Show task list.')
    print('3. Mark a task as DONE.')
    print('4. Exit')


def run_app():
    while True:
        menu()
        option = int(input('Ke vrei sa faki?!: '))
        if option == 1:
            add_task()
        if option == 2:
            show_task_list()
        if option == 3:
            mark_as_done()
        if option == 4:
            print('App closed! Go home!')
            break


create_db()
create_table()
run_app()