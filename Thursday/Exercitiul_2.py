import mysql.connector as mysql
from datetime import datetime


def create_structure():
    conn = mysql.connect(
        host="localhost",
        user="root",
        password="Blackmetal15$",
        database="",
    )

    with conn.cursor() as c:
        c.execute("CREATE DATABASE IF NOT EXISTS shop;")
        c.execute("""
            CREATE TABLE IF NOT EXISTS shop.utilizator (
                id INT PRIMARY KEY AUTO_INCREMENT,
                nume TEXT NOT NULL,
                email TEXT NOT NULL,
                parola TEXT NOT NULL
            );
        """)
        c.execute("""
            CREATE TABLE IF NOT EXISTS shop.produs (
                id INT PRIMARY KEY AUTO_INCREMENT,
                denumire TEXT NOT NULL,
                cantitate INT NOT NULL,
                pret DECIMAL(8, 2) NOT NULL 
            );
        """)
        c.execute("""
            CREATE TABLE IF NOT EXISTS shop.istoric (
                id INT PRIMARY KEY AUTO_INCREMENT,
                user_id INT NOT NULL,
                produs_id INT NOT NULL,
                data TIMESTAMP NOT NULL,
                cantitatea_cumparata INT NOT NULL,
                CONSTRAINT fk_user_id FOREIGN KEY(user_id) REFERENCES shop.utilizator(id),
                CONSTRAINT fk_produs_id FOREIGN KEY(produs_id) REFERENCES shop.produs(id)
            );
        """)

    conn.close()


create_structure()


def show_menu_1():
    print("1. Login")
    print("2. Register")
    print("0. Exit application")


def show_menu_2():
    print("1. Show product list")
    print("2. Buy product")
    print("3. Show history")
    print("4. Logout")
    print("0. Exit application")

user_logat = None

def register():
    nume = input('Enter your name: ')
    email = input('Enter your email: ')

    password = input('Enter your password: ')
    with conn.cursor() as c:
        c.execute("INSERT INTO shop.utilizator (nume, email, password) VALUES (%s, %s, %s );",
                  (nume, email, password))
        conn.commit()

def login_user():
    email = input('Enter your email: ')
    password = input('Enter your password: ')
    with conn.cursor() as c:
        c.execute('SELECT id FROM shop.utilizator WHERE email = %s AND password = %s;', (email, password))
        result = c.fetchone()
        if result:
            user_logat = result[0]
        else:
            print(f'Adresa de email sau parola au fost introduse gresit')

def show_product_list():
    with conn.cursor() as c:
        c.execute('SELECT denumire, pret, cantitate FROM produs WHERE cantitate > 0')
        results = c.fetchall()
        for result in results:
            print(result)

def user_option():
    option = int(input('Enter your option: '))
    if option == 1:
        show_product_list()

def buy_product():
    id_produs = int(input('Enter product id: '))
    cantitate = int(input('Enter quantity: '))
    with conn.cursor() as c:
        c.execute('SELECT * FROM produs WHERE id=%s AND cantitate >= %s', (id_produs, cantitate))
        result = c.fetchone()
        if result:
            c.execute('INSERT INTO istoric (user_id, produs_id, data, cantitatea_cumparata', (user_logat, id_produs, datetime.now))


def shop.istoric():
    with conn.cursor() as c:
        c.execute('SELECT produs_id, cantitatea_cumparata FROM istoric INNER JOIN  WHERE user_id = %s ;',(user_logat, ))



